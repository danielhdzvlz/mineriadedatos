clear
close all
%1.-Leer una imagen a color
im=imread('img/periq.jpg'); [ancho,largo,~]=size(im);   figure,imshow(im);  X=reshape(im,[],3);
NumCluster=25;%randi(10);
Iteraciones=140;fprintf(strcat('Grupos: ',num2str(NumCluster),'\n'));
%---------------------------------------------------------------------------------------%
[id,C]=kmeans(double(X),NumCluster,'MaxIter',Iteraciones);
X2=[;]; C=round(C);
for i=1:length(X)
    X2(i,:)=C(id(i),:);  
end
ImagenKmeans=reshape(X2,[ancho,largo,3]);   figure,imshow(uint8(ImagenKmeans));
%---------------------------------------------------------------------------------------%
[Cf,U]=fcm(double(X),NumCluster,[NaN;Iteraciones;NaN;0]);
X3=[;]; Cf=round(Cf);
maxU = max(U);
for i=1:length(X)
    for j=1:NumCluster
        aux=U(j,i);
        if(aux==maxU(i))
            X3(i,:)=Cf(j,:);  
        end
    end
end
ImagenFuzzyKmeans=reshape(X3,[ancho,largo,3]);  figure,imshow(uint8(ImagenFuzzyKmeans));

