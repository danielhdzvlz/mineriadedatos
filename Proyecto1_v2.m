clear
close all
imagenrgb = imread('peppers.png');[ancho,largo,~]=size(imagenrgb);
%imagenlab = rgb2lab(imagenrgb);X=reshape(imagenlab,[],3);figure,imshow(imagenlab(:,:,1),[0 100])
imagenHSV = rgb2hsv(imagenrgb);X=reshape(imagenHSV,[],3);
figure,imshow(imagenHSV);  
figure,imshow(imagenrgb);  
NumCluster=25;%randi(10);
Iteraciones=140;fprintf(strcat('Grupos: ',num2str(NumCluster),'\n'));
%---------------------------------------------------------------------------------------%
[id,C]=kmeans(double(X),NumCluster,'MaxIter',Iteraciones);
X2=[;]; C=round(C);
for i=1:length(X)
    X2(i,:)=C(id(i),:);  
end
%X2 = lab2rgb(X2);
%X2 = hsv2rgb(X2);
ImagenKmeans=reshape(X2,[ancho,largo,3]);   figure,imshow(uint8(ImagenKmeans));
%---------------------------------------------------------------------------------------%
[Cf,U]=fcm(double(X),NumCluster,[NaN;Iteraciones;NaN;0]);
X3=[;]; Cf=round(Cf);
maxU = max(U);
for i=1:length(X)
    for j=1:NumCluster
        aux=U(j,i);
        if(aux==maxU(i))
            X3(i,:)=Cf(j,:);  
        end
    end
end
index1 = find(U(1,:) == maxU);
index2 = find(U(2,:) == maxU);
%X3 = lab2rgb(X3);
ImagenFuzzyKmeans=reshape(X3,[ancho,largo,3]);  figure,imshow(uint8(ImagenFuzzyKmeans));